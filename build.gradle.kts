import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

@Suppress("DSL_SCOPE_VIOLATION") // Works around an IntelliJ bug.
plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.shadow)
    alias(libs.plugins.plugin.yml)
}

group = "org.example"
version = "1.0-SNAPSHOT"

bukkit {
    name = "ExamplePlugin"
    apiVersion = "1.19"
    main = "org.example.plugin.PaperExamplePlugin"

    description = "An example Paper plugin."
    authors = listOf("You!")

    commands {
        register("hello") {
            description = "A simple test command."
        }
    }
}

repositories {
    mavenCentral()
    maven("https://repo.papermc.io/repository/maven-public/")
}

dependencies {
    // Paper API.
    compileOnly(libs.paper.api)

    // Shadow the Kotlin standard library.
    shadow(libs.kotlin.stdlib)
}

kotlin {
    jvmToolchain(17)
}

tasks {
    build {
        dependsOn(shadowJar)
    }

    withType<ShadowJar>().configureEach {
        configurations {
            project.configurations.shadow
        }

        // Remove the archive classifier.
        archiveClassifier.set("")

        relocate("kotlin", "${project.group}.shadow.kotlin")
        relocate("kotlinx", "${project.group}.shadow.kotlinx")
        relocate("org.jetbrains", "${project.group}.shadow.jetbrains")
        relocate("org.intellij", "${project.group}.shadow.intellij")
    }
}