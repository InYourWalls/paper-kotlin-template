# paper-kotlin-template

A template for a [Paper](https://papermc.io/) plugin, written in Kotlin (using
the [plugin-yml](https://github.com/minecrell/plugin-yml) Gradle plugin).

## Usage

1. Clone the repository into a folder for your plugin.
2. Update `build.gradle.kts` so that the maven group and project version reflect those of your plugin.
3. Rename the directories and update the `package` statements to reflect your plugin's maven group.
4. Update the metadata in the `bukkit` block within `build.gradle.kts` so that it is correct for your plugin.
5. Update `settings.gradle.kts` so that the project name reflects your plugin.
6. Finally, add/modify any dependencies in `gradle/libs.versions.toml`.
7. Start coding!
