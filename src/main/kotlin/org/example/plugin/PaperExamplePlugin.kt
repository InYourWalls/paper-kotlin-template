// A simple Paper example plugin.
package org.example.plugin

import net.kyori.adventure.text.Component
import org.bukkit.plugin.java.JavaPlugin

class PaperExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        // Register a simple test command.
        getCommand("hello")?.setExecutor { sender, _, _, _ ->
            sender.sendMessage {
                Component.text("Hello!")
            }

            true
        }

        // Say hello.
        slF4JLogger.info("Hello, Paper!")
    }
}